<?php

// Load Core Files
include_once 'Config.php';

// Auto Loader Core
spl_autoload_register(function($className){
    require_once $className . '.php';
});