<?php

class Pages extends Controller {
    public function __construct(){
        
    }

    public function index() {
        $data = [
            'title' => 'Prep funding'
        ];
        $this -> view('pages/index', $data);
    }

    public function about(){
        $data = [
            'title' => 'About US'
        ];
        $this -> view('pages/about', $data);
    }

    public function login() {
        echo 'Login Page';
    }
}