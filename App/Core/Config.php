<?php
// Connection to Database
define('DB_HOST', 'localhost');
define('DB_USER', 'prepfund');
define('DB_PASSWORD', 'prepfund');
define('DB_NAME', 'prepfund');

// APPROOT
define('APPROOT', dirname(dirname(__FILE__)));

//URL ROOT
define('URLROOT', 'https://localhost/prepfund');

//SITE NAME
define('SITENAME', 'Prep fund');
define('SLOGAN', 'Prepare for funding');
define('VERSION', '1.0.1');