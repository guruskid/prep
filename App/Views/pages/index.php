<?php require APPROOT . '/Views/inc/header.php' ?>
<div class="jumbotron">
    <h1><?php echo $data['title'] ?></h1>
    <p class="lead">This example is a quick exercise to illustrate how the top-aligned navbar works. As you scroll, this navbar remains in its original position and moves with the rest of the page.</p>
    <a class="btn btn-lg btn-primary" href="../../components/navbar/navbar.htm" role="button">View navbar docs &raquo;</a>
</div>
<?php require APPROOT . '/Views/inc/footer.php' ?>